import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
   const tasks = [{
      id: 1,
      title: 'Ir ao mercado',
      date: '2024-02-27',
      time: '10:00',
      location: 'Supermercado São Paulo',
   }, {
      id: 2,
      title: 'Fazer exercícios',
      date: '2024-02-28',
      time: '15:30',
      location: 'Academia Fitness Plus',
   }, {
      id: 3,
      title: 'Reunião de trabalho',
      date: '2024-03-01',
      time: '09:00',
      location: 'Escritório ABC',
   }]

   const taskPress = (task) => {
      navigation.navigate('DetalhesDasTarefas', { task })
   }
   return (
      <View>
         <FlatList
            data={tasks}
            keyExtractor={(item) => item.id.toString}
            renderItem={({ item }) => (
               <TouchableOpacity onPress={() => taskPress(item)}>
                  <Text>{item.title}</Text>
               </TouchableOpacity>
            )}
         />
      </View>
   )
}

export default TaskList;
